# docker
Docker images - mostly dev environments for VS Code containers

Go to [@alexdaiii-docker](https://github.com/alexdaiii-docker) to get the link to the images that ultimately get built on Dockerhub.
